CHANGES
=======

1.26.0
------

* Add a note about contribution priorities
* Add a note on Windows and Unicode
* Fix output printing with python3
* Handle http queries below /
* Support git 2.15 and newer
* show the config value result after fetching it in verbose mode
* Actually output the warning
* Fix listing changes over SSH for 2.14
* Provide link to MediaWiki platform specific page
* Better username detection for add\_remote()
* Refactor displaying of reviews
* Added topic field to the list output
* Switch to string format to avoid interpolation issues
* Refactor Isolated Env to use in unit tests
* Set author and committer explicitly
* Use hash of test ID to pick Gerrit ports in tests
* Clarify that submitting multiple commits is OK
* Remove discover from test-requirements
* Install bc libs when running testsuite
* Add several missing options to the man page
* Fix AttributeError when can not connect to Gerrit
* Set a default EXIT\_CODE for GitReviewException
* Use consistent formatting for deprecations
* Fix git-review -d behavior with branches
* Support git without git credential
* Remove worthless print
* Fix no\_git\_dir UnboundLocalError in except block
* Update gerrit version for testing
* Remove argparse from requirements
* Correct metadata URLs
* Avoid AttributeError when raising raw GitReviewExceptions
* fix encoding issue on Windows
* Fix one typo on git-review documentation
* Use git push-url instead of a second remote
* Don't parse git log header for topic
* Ignore .eggs cruft
* Fix H238 violation and enable check for that rule
* Update to newer hacking checks
* Remove spurious mdoc directives

1.25.0
------

* Add “git-review -l” to man page synopsis and usage doc
* Add reviewers on upload
* Update project links
* Override E-mail in Gerrit integration tests
* Fixed a dead link to the git-review docs
* Provide an explanation on failed rebase
* Switch to requests to support proxying of 'https'
* Use plumbing rev-parse to get the branch name
* Isolate tests from user/system git configuration
* Push language override down into the cmd wrapper
* git review -sv gets more verbose
* Add utf-8 char support
* Choose tracked branch for rebase when submitting
* pbr should install the manpage
* get\_remote\_url(): also honor url.\*.pushInsteadOf
* Support authentication in run\_http\_exc
* Split README.rst into separate doc files
* Handle correctly http error raise in run\_http\_exc
* Fix encoding header
* Workflow documentation is now in infra-manual
* Update tests to use Gerrit 2.9.2
* Use 'no\_proxy' env variable in addition to uppercase
* Enable color support based on tty and config
* get\_remote\_url(): honor any "url.<base>.insteadOf" config setting
* Convert add\_remote to use GitReviewExceptions
* Prefer git-config over git-review config files
* Isolate tests from user/system config
* Switched documentation to point to storyboard
* Fix ---list and "departement" typos in man page
* Align git-review and python -m git\_review.cmd behaviors
* Define -T/--no-topic to disable review submit with topic
* Work toward Python 3.4 support and testing
* -F/--force-rebase has no effect if rebase is disabled by config
* Remove useless constants
* Improve windows support for git-review --setup
* Fix groff warnings with manpage
* Enabled hacking checks H305 and H307
* Prevent long subjects in reviews causing spurious blank lines
* added link to get-pip to readme
* Disable ssh/scp password authentication during tests
* Update tests to use Gerrit 2.8.6.1
* Build universal wheels

1.24
----

* Update homepage on PyPI
* Update requirements to OpenStack's recommendations
* Update the README to mention dependencies
* Avoid a stacktrace when no reviews are pending
* Ensure username is set for all tests
* Provide nicer user message for missing remote ref
* Fix a typo in HACKING.rst
* Ignore newline in bp/bug search in commit message
* Restrict tests SSH auth to only the provided key
* Disable proxies for tests that clone over http
* Keep track of gerrit.war and golden\_site versions
* Fix typo in manpage s/gireview/gitreview/
* Correct git review -l over http(s)
* Topic: do not use '(detached' when detached
* Use gerrit 2.8.5 instead of gerrit 2.6.1 in tests
* Allow to specify default scheme in .gitreview file
* Correct test\_remote to support branchs without upstream
* Remove parsing of --help and variants from our code
* Python2: fixed UnicodeEncodeError
* Skip invalid unicode in commit messages
* Git review assumes the wrong ssh default port
* Add http(s) protocol support to fetch\_review and list\_reviews
* git-review.1 manpage fix for groff warnings
* Fix parsing of SCP-style URLs, as these are valid in Git itself
* "git review --setup" failed in Chinese locale
* Bump hacking version in requirements
* Reduce testr concurrnecy to 2
* Add http(s) protocol support to set\_hooks\_commit\_msg
* Retrieve remote pushurl independently of user's locale
* Add http(s) protocol support to test\_remote
* Verify if every attached file exists. Attach gerrig.config
* Wrap exceptions that occur while running external process
* Make Gerrit port and dir selection deterministic
* Don't try to attach known\_hosts if it's not there
* Remove tox locale overrides
* Fix the regex for setting topic
* Add 2m timeout to tests
* Attach Gerrit logs and known\_hosts to failed tests
* Change test gerrit ssh/http ports offset
* Correct .Fl typo WRT --compare in the manual page
* Ignore content of merge commits in reporting
* Remove empty lines from git log output
* Preserve merges when doing a rebase
* Split git rev-parse --show-toplevel --git-dir on newline
* Prefer .gitconfig username
* Add more deterministic port selection for Gerrit
* Document source location as git.openstack.org
* Implement integration tests
* Migrate to pbr
* No longer check for new git-review releases

1.23
----

* Wrap long lines
* Pin hacking <0.6
* Fix str(None) bug in port detection for hook setup
* Fix pep8 deps for pyflakes version conflict
* Expand multiple commit submission warning
* Start development of 1.23

1.22
----

* Provide usage help even if not in Git directory
* Document defaultremote option & site/user configs
* Allow per-site and per-user Gerrit defaults
* Rename README.md to README.rst
* Add venv testenv to tox.ini
* Start development of 1.22

1.21
----

* Align to OpenStack Hacking guidelines
* Switch to flake8 from pep8
* Allow per-user override of -R setting
* git\_config\_get\_value to return None
* Use the local branch name as topic only if it is different from remote
* Jeremy's manpath workaround breaks on Fedora
* Compare different Patch Sets of Review
* bug/1144800: Fix fatal: both revision and filename
* Changed the text of the manpage to read "make it easier to comprehend Gerrit"
* Don't call get\_branch\_name from assert\_one\_change
* Add custom scripts features
* Download specific Patch Set for Review
* Make README.md ReST-friendly
* Document gitreview.username
* Better determine git directories
* Don't fetch remote outside of rebase flow
* Make it possible to cherry-pick a change
* Check HEAD for extra commits instead of master
* Check that the owner of a change has name defined
* Topic name should be determined only for git push
* Fix regression in repeated -d of the same change
* Remove two else: blocks in main()
* Update README for project move
* Updated .gitreview location
* Add mailing list to README
* Use exceptions for list\_reviews
* Use exceptions for finish\_review
* Use exceptions for download\_review
* git-review(1): explain exit code ranges
* Introduce base exception classes
* Follow up I92b8637c: Fix Python 3 compatibility
* Start development on 1.21
* Remove reference to nonexistent requirements.txt

1.20
----

* Avoid symlinks in the manpage path
* Start development on 1.20

1.19
----

* Revert "Introduce base exception classes"
* Introduce base exception classes
* Revert "Introduce base exception classes"
* Revert "git-review(1): explain exit code ranges"
* git-review(1): explain exit code ranges
* Introduce base exception classes
* Review list fields should have constant widths
* manpage minor fixes with no output changes
* Make setup.py less Linux-specific, more UNIX-compliant
* Fixing ponctuation issue
* Introduce CommandFailed exception for run\_command
* Use run\_command\_status \*argv for ssh and scp
* Refactor run\_command to use \*args, \*\*kwargs
* Get rid of "status" parameter in run\_command
* Due to gerrit bug regeneration of Change-Id must be nessecary
* Don't rebase if the rebase doesn't conflict
* Allow download of reviews from deleted accounts
* Python 3 compatibility fixes
* Add flag to push to refs/for/\* for compatibilty
* Add Python 3 support
* Just fixing a small typo
* Revert to 1.17
* Configure a pep8 tox environment
* Fixes typos and omission in failed review list
* Allow download of reviews from deleted accounts
* Start development on 1.19

1.18
----

* Reversed the is:reviewable logic
* Add ability to upload as work in progress
* Filter list by is:reviewable by default
* Due to gerrit bug regeneration of Change-Id must be nessecary
* Don't rebase if the rebase doesn't conflict
* Add open action
* Add setup, list and download actions
* Get current branch more concisely
* fix missing username attribute in JSON stream
* Rename submit action to upload
* Fix no-change detection
* Fix pep8 errors with 1.3.1
* Add optional 'submit' positional argument
* Add flag to push to refs/for/\* for compatibilty
* Add review scores to listing
* Add Review, PatchSet and Hacker classes
* Return a json result iterator from run\_ssh\_query()
* Only list reviews for the current branch
* Refactor out run\_ssh\_query() method
* Add Python 3 support
* Just fixing a small typo
* Start dev on 1.18

1.17
----

* Fixed hook installation for git with submodules
* Update publish ref from refs/for to refs/publish
* Run 'git remote update' from assert\_one\_change
* Fix --list breakage in 3531a5bffd4f
* Disable ssh X11 forwarding
* Add support to list changes for review
* Removed parsing out of team
* fix -d not reusing already existing local branch
* Start development on 1.17

1.16
----

* Change draft ref to drafts
* Fix scope error with configParser
* Retrieve project & team name from fetch url
* minor glitches in manpage
* More resilient downloading
* Override remote options: update manpage
* Reformat manpage to mdoc(7)
* Allow the user to override remote consequently
* enhance man page
* bump year references from 2011 to 2012
* Start development on 1.16

1.15
----

* Actually fix the urlparse bug in OSX
* Start dev on 1.15

1.14
----

* Fix an obvious breakage on OSX
* Start dev on 1.14

1.13
----

* Workaround for OSX urlparse issue
* Include timing info in verbose output
* Remove automagic requirements.txt
* Use dirname instead of basename
* accepts color.ui boolean values
* Started 1.13

1.12
----

* Provide easy flag for disabling update check
* Freeze requirements for sdist packages
* Start 1.12

1.11
----

* Start 1.11

1.10
----

* Added tox support for testing pep8
* Added dependencies in requirements.txt
* Add myself to AUTHORS
* Add support for Gerrit 2.2.2's "Draft" status
* Remove useless username parameter from add\_remote
* Make the default branch configurable
* Break out the config reading into its own method
* Don't hardcode 'master'
* Fix typo (Ammending -> Amending)
* Switch from 'git show' to 'git log'
* Migrate to argparse. optparse is deprecated
* Make readme links clickable
* Add installation instructions to readme
* Clarify instructions for contributing
* Add .gitreview instructions to readme
* Post 1.9 release version bump

1.9
---

* Remove commit --amend from hook installation
* Bump version post release

1.8
---

* Prep for release of 1.8
* Document the new command line options
* Only pass plain strings to shlex.split
* Add --finish option to git-review
* Remove the commands module and use subprocess
* Add workaround for broken urlparse
* Install hook via scp rather than http
* Use specified remote for getting commit hook
* Fix bug 901030

1.7
---

* Use fetch/checkout instead of pull
* Only use color on systems that support it
* Create .git/hooks directory if not present
* Version bump post release

1.6
---

* Check git config for default username
* Handle usernames better in remote setup
* Added Saggi to AUTHORS file. Thanks Saggi!
* Spruced up the README just a bit
* Show branches and tags when showing git log
* Better handling of the color git configuration
* commit\_msg declared but never used
* In detached head, use the target branch as topic
* Always show the list of outstanding commits
* Fix multi-change confirmation testing
* Force single-version-externally-managed
* Removed old doc from MANIFEST.in
* setup.py should point to launchpad project page
* Check to ensure that only one change is submitted
* Post release version bump

1.5
---

* Oops. Left git-review.1 out of the MANIFEST.in

1.4
---

* Replace sphinx manpage with manual one
* Bump version after release

1.3
---

* Clean up documentation; add --version
* Bump version after release

1.2
---

* Better guessing of username
* Check .gitreview file in repo for location of gerrit
* fixed problem downloading changes
* Replace git fetch with git remote update
* Add --setup command for a pro-active repo setup
* Updated docs to describe setup
* Wrap sphinx calls in a try/except block
* Update version after release

1.1
---

* Replace tuple with list to unconfuse setuptools
* Added support for fixing existing gerrit remotes
* Always run git fetch so that commits are available for rebase
* Pull version in directly from git-review
* Fix manpage installation for setup.py install
* Add remote arg to git remote update
* Install man pages
* Added support for download command
* Handle repos cloned from github git urls
* Fix git push command
* Bumped version after release to PyPI

1.0
---

* Support openstack-ci github organization
* Added the remote option and updated README
* Bug fixes relating to first-time runs
* Added support for updating
* Ported rfc.sh to a standalone program
